import babel from '@rollup/plugin-babel'
import nodeResolve from '@rollup/plugin-node-resolve'
import typescript from '@rollup/plugin-typescript'
import fs from 'fs/promises'
import path from 'path'
import { defineConfig } from 'rollup'
import { fileURLToPath } from 'url'

const __dirname = path.dirname(fileURLToPath(import.meta.url))

async function getPackagePaths() {
  const packageRootDir = path.join(__dirname, 'packages')
  const packages = await fs.readdir(packageRootDir)

  return packages.map((packageName) => {
    const projectDir = path.join(packageRootDir, packageName)
    const sourceDir = path.join(projectDir, 'src')
    const outputDir = path.join(projectDir, 'dist')

    return { projectDir, sourceDir, outputDir }
  })
}

function getBanner(packageName, version) {
  return `/**
 * ${packageName} v${version}
 *
 * Copyright (c) BuildLine GmbH
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.md file in the root directory of this source tree.
 *
 * @license MIT
 */`
}

export default defineConfig(
  await (async () => {
    const packages = await getPackagePaths()

    return await Promise.all(
      packages.map(async ({ projectDir, sourceDir, outputDir }) => {
        const packageJson = JSON.parse(
          await fs.readFile(path.join(projectDir, 'package.json'))
        )
        const packageName = packageJson.name
        const packageVersion = packageJson.version ?? '0.0.0'

        const banner = getBanner(packageName, packageVersion)

        /** @type {import('rollup').RollupOptions} */
        const cjsOptions = {
          external: (id) => {
            return !id.startsWith('.') && !path.isAbsolute(id)
          },
          input: path.resolve(sourceDir, 'index.ts'),
          output: [
            {
              format: 'cjs',
              banner,
              dir: outputDir,
              exports: 'named',
              sourcemap: true,
              preserveModules: true,
            },
          ],
          plugins: [
            babel({
              babelHelpers: 'bundled',
              exclude: /node_modules/,
              extensions: ['.ts', '.tsx'],
            }),
            nodeResolve({ extensions: ['.ts', '.tsx'] }),
            typescript({
              declaration: true,
              declarationDir: outputDir,
              outputToFilesystem: true,
              tsconfig: path.resolve(projectDir, 'tsconfig.json'),
            }),
          ],
        }

        const esmOptions = {
          ...cjsOptions,
          output: {
            format: 'esm',
            banner,
            dir: path.join(outputDir, 'esm'),
            exports: 'named',
            sourcemap: true,
            preserveModules: true,
          },
          plugins: cjsOptions.plugins.slice(0, -1),
        }

        /** @type {import('rollup').RollupOptions} */
        return [cjsOptions, esmOptions]
      })
    ).then((c) => c.flat(1))
  })()
)
