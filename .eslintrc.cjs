/** @type {import('eslint').Linter.Config} */
module.exports = {
  plugins: ['simple-import-sort', 'sonarjs', '@typescript-eslint'],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:sonarjs/recommended',
  ],
  env: {
    node: true,
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: ['./tsconfig.eslint.json', './packages/*/tsconfig.json'],
    tsconfigRootDir: __dirname,
  },
  rules: {
    'simple-import-sort/imports': 'warn',
    // Was too annoying with a lot of false positives for translation strings
    'sonarjs/no-duplicate-string': 'off',
    // TypeScript throws a compilation error if there are code paths in an
    // array.map() or similar methods that don't return. ESLint gave false
    // positives when using exhaustive switch statements
    'array-callback-return': 'off',
    // We'll allow unused variables as long as they start with an underscore
    '@typescript-eslint/no-unused-vars': [
      'warn',
      { varsIgnorePattern: '^_.*$', ignoreRestSiblings: true },
    ],
    '@typescript-eslint/consistent-type-imports': 'warn',
    '@typescript-eslint/no-explicit-any': 'off',
    // When working with unions in TypeScript, having a switch might make more
    // sense because you'll get compiler errors when not all cases are handled
    // which an if/else would not catch
    'sonarjs/no-small-switch': 'off',
    '@typescript-eslint/switch-exhaustiveness-check': 'error',
    '@typescript-eslint/no-extra-semi': 'off',
    '@typescript-eslint/no-empty-function': 'off',
  },
  root: true,
}
