import type { Static, TSchema } from '@sinclair/typebox'
import { TypeCompiler } from '@sinclair/typebox/compiler'
import type {
  FormPath,
  SchemaValidationError,
  SchemaValidator,
} from '@typed-form-validation/core'
import { Result } from 'neverthrow'
import { err, ok } from 'neverthrow'

import type { TypeboxSchemaValidationError } from './schema-validation-error'
import { schemaValidationErrorFromValueError } from './schema-validation-error'

export class ValidationError<E> extends Error {
  constructor(public readonly errors: E[]) {
    super('ValidationError')
  }
}

export interface TypeboxSchemaValidator<
  Schema extends TSchema,
  CustomError extends SchemaValidationError | never = never,
  SkipTypeValidation extends boolean = false
> extends SchemaValidator<
    Static<Schema>,
    CustomError,
    false extends SkipTypeValidation ? any : Static<Schema>
  > {
  schema: Schema
}

export function typeboxSchemaValidator<
  Schema extends TSchema,
  CustomError extends SchemaValidationError | never = never
>(
  schema: Schema,
  isSemanticallyValid: (
    value: Static<Schema>
  ) => Result<Static<Schema>, CustomError[]>,
  options: { skipTypeValidation: true }
): TypeboxSchemaValidator<
  Schema,
  TypeboxSchemaValidationError | CustomError,
  true
>

export function typeboxSchemaValidator<
  Schema extends TSchema,
  CustomError extends SchemaValidationError | never = never
>(
  schema: Schema,
  isSemanticallyValid?: (
    value: Static<Schema>
  ) => Result<Static<Schema>, CustomError[]>
): TypeboxSchemaValidator<
  Schema,
  TypeboxSchemaValidationError | CustomError,
  false
>

export function typeboxSchemaValidator<
  Schema extends TSchema,
  CustomError extends SchemaValidationError | never = never
>(
  schema: Schema,
  isSemanticallyValid?: (
    value: Static<Schema>
  ) => Result<Static<Schema>, CustomError[]>,
  options?: { skipTypeValidation: true }
): TypeboxSchemaValidator<Schema, TypeboxSchemaValidationError | CustomError> {
  const compiled = TypeCompiler.Compile(schema)

  function isType(value: unknown): value is Static<Schema> {
    if (!compiled.Check(value)) {
      return false
    }
    if (!isSemanticallyValid) return true
    return isSemanticallyValid(value).isOk()
  }

  function assertIsType(value: unknown): asserts value is Static<Schema> {
    const result = validate(value)
    if (result.isErr()) {
      throw new ValidationError(result.error)
    }
  }

  function asType(value: unknown): Static<Schema> {
    assertIsType(value)
    return value
  }

  function validate(
    value: unknown
  ): Result<Static<Schema>, Array<TypeboxSchemaValidationError | CustomError>> {
    const errors = [...compiled.Errors(value)].map(
      schemaValidationErrorFromValueError
    )
    const result: Result<
      Static<Schema>,
      (TypeboxSchemaValidationError | CustomError)[]
    > = errors.length > 0
      ? err(errors as Array<TypeboxSchemaValidationError | CustomError>)
      : ok(value as Static<Schema>)

    if (options?.skipTypeValidation !== true) {
      return isSemanticallyValid ? result.andThen(isSemanticallyValid) : result
    }

    const checkSemanticValidity = (): Result<void, CustomError[]> => {
      if (!isSemanticallyValid) return ok(void 0)
      try {
        return isSemanticallyValid(value as Static<Schema>).map(() => {})
      } catch (error) {
        return err([])
      }
    }

    return Result.combineWithAllErrors([result, checkSemanticValidity()])
      .map(([value]) => value)
      .mapErr((errors) => errors.flat(1))
  }

  return {
    schema,
    isSemanticallyValid: isSemanticallyValid || ((value) => ok(value)),
    isType,
    assertIsType,
    asType,
    validate,
  }
}

export function validateSubschema<T, CustomError extends SchemaValidationError>(
  prefix: FormPath,
  value: T,
  subschemaValidator: {
    isSemanticallyValid: (value: T) => Result<T, CustomError[]>
  }
): Result<T, CustomError[]> {
  return subschemaValidator.isSemanticallyValid(value).mapErr((errors) =>
    errors.map((error) => ({
      ...error,
      path: prefix.length > 0 ? prefix.concat(error.path) : error.path,
    }))
  )
}
