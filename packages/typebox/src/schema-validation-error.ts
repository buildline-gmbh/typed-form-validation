import type { ValueError } from '@sinclair/typebox/value'
import { ValueErrorType } from '@sinclair/typebox/value'
import type { SchemaValidationError } from '@typed-form-validation/core'

/**
 * Subset of typebox ValueErrorTypes that are most relevant for us.
 *
 * @see {ValueErrorType}
 */
export type TypeboxSchemaValidationError = SchemaValidationError<
  'schema',
  | { type: 'expected-array' }
  | { type: 'expected-array-length-gte'; minItems: number }
  | { type: 'expected-array-length-lte'; maxItems: number }
  | { type: 'expected-array-unique-items' }
  // `BigInt` errors skipped
  | { type: 'expected-date' }
  | { type: 'expected-date-gt'; exclusiveMinimum: number }
  | { type: 'expected-date-gte'; minimum: number }
  | { type: 'expected-date-lt'; exclusiveMaximum: number }
  | { type: 'expected-date-lte'; maximum: number }
  | { type: 'expected-int' }
  | { type: 'expected-int-multiple-of'; multipleOf: number }
  | { type: 'expected-int-gt'; exclusiveMinimum: number }
  | { type: 'expected-int-gte'; minimum: number }
  | { type: 'expected-int-lt'; exclusiveMaximum: number }
  | { type: 'expected-int-lte'; maximum: number }
  // `Intersect` skipped
  | { type: 'expected-literal'; literal: string }
  // `Never` skipped
  | { type: 'expected-not' }
  | { type: 'expected-null' }
  | { type: 'expected-number' }
  | { type: 'expected-number-multiple-of'; multipleOf: number }
  | { type: 'expected-number-gt'; exclusiveMinimum: number }
  | { type: 'expected-number-gte'; minimum: number }
  | { type: 'expected-number-lt'; exclusiveMaximum: number }
  | { type: 'expected-number-lte'; maximum: number }
  | { type: 'expected-object' }
  | { type: 'expected-object-min-props'; minProperties: number }
  | { type: 'expected-object-max-props'; maxProperties: number }
  | { type: 'expected-object-not-to-have-prop' }
  | { type: 'expected-object-required-prop' }
  // `Promise` skipped
  | { type: 'expected-record-numeric-keys' }
  | { type: 'expected-record-string-keys' }
  | { type: 'expected-string' }
  | { type: 'expected-string-length-gte'; minLength: number }
  | { type: 'expected-string-length-lte'; maxLength: number }
  | { type: 'expected-string-pattern'; pattern: string }
  // `StringFormatUnknown` skipped
  | { type: 'expected-string-format'; format: string }
  // `Symbol` skipped
  // `Tuple` errors skipped
  | { type: 'expected-undefined' }
  | { type: 'expected-union' }
  // `Uint8Array` errors skipped
  // `void` skipped
  // `Custom` skipped
  // all other errors are folded into this error
  | { type: 'other'; message: string }
> // & { schema: TSchema }

function createErr<T extends TypeboxSchemaValidationError['type']>(
  type: T,
  data: Omit<
    TypeboxSchemaValidationError & { type: T },
    'schema' | 'source' | 'type'
  >
): TypeboxSchemaValidationError {
  return {
    source: 'schema',
    type,
    ...data,
  } as unknown as TypeboxSchemaValidationError
}

export function schemaValidationErrorFromValueError(
  error: ValueError
): TypeboxSchemaValidationError {
  const { path: valuePath, schema } = error
  // convert from /this/path/schema to ['this', 'path', 'schema']
  const path = valuePath.replace(/^\//, '').split('/')

  // eslint-disable-next-line sonarjs/max-switch-cases
  switch (error.type) {
    case ValueErrorType.Array:
      return createErr('expected-array', { path })
    case ValueErrorType.ArrayMinItems:
      return createErr('expected-array-length-gte', {
        path,
        minItems: schema.minItems,
      })
    case ValueErrorType.ArrayMaxItems:
      return createErr('expected-array-length-lte', {
        path,
        maxItems: schema.maxItems,
      })
    case ValueErrorType.ArrayUniqueItems:
      return createErr('expected-array-unique-items', { path })
    case ValueErrorType.Date:
      return createErr('expected-date', { path })
    case ValueErrorType.DateExclusiveMinimumTimestamp:
      return createErr('expected-date-gt', {
        path,
        exclusiveMinimum: schema.exclusiveMinimum,
      })
    case ValueErrorType.DateExclusiveMaximumTimestamp:
      return createErr('expected-date-lt', {
        path,
        exclusiveMaximum: schema.exclusiveMaximum,
      })
    case ValueErrorType.DateMinimumTimestamp:
      return createErr('expected-date-gte', {
        path,
        minimum: schema.minimum,
      })
    case ValueErrorType.DateMaximumTimestamp:
      return createErr('expected-date-lte', {
        path,
        maximum: schema.maximum,
      })
    case ValueErrorType.Integer:
      return createErr('expected-int', { path })
    case ValueErrorType.IntegerMultipleOf:
      return createErr('expected-int-multiple-of', {
        path,
        multipleOf: schema.multipleOf,
      })
    case ValueErrorType.IntegerExclusiveMinimum:
      return createErr('expected-int-gt', {
        path,
        exclusiveMinimum: schema.exclusiveMinimum,
      })
    case ValueErrorType.IntegerExclusiveMaximum:
      return createErr('expected-int-lt', {
        path,
        exclusiveMaximum: schema.exclusiveMaximum,
      })
    case ValueErrorType.IntegerMinimum:
      return createErr('expected-int-gte', {
        path,
        minimum: schema.minimum,
      })
    case ValueErrorType.IntegerMaximum:
      return createErr('expected-int-lte', {
        path,
        maximum: schema.maximum,
      })
    case ValueErrorType.Literal:
      return createErr('expected-literal', {
        path,
        literal:
          typeof schema.const === 'string' ? `'${schema.const}'` : schema.const,
      })
    case ValueErrorType.Not:
      return createErr('expected-not', { path })
    case ValueErrorType.Null:
      return createErr('expected-null', { path })
    case ValueErrorType.Number:
      return createErr('expected-number', { path })
    case ValueErrorType.NumberMultipleOf:
      return createErr('expected-number-multiple-of', {
        path,
        multipleOf: schema.multipleOf,
      })
    case ValueErrorType.NumberExclusiveMinimum:
      return createErr('expected-number-gt', {
        path,
        exclusiveMinimum: schema.exclusiveMinimum,
      })
    case ValueErrorType.NumberExclusiveMaximum:
      return createErr('expected-number-lt', {
        path,
        exclusiveMaximum: schema.exclusiveMaximum,
      })
    case ValueErrorType.NumberMinumum:
      return createErr('expected-number-gte', {
        path,
        minimum: schema.minimum,
      })
    case ValueErrorType.NumberMaximum:
      return createErr('expected-number-lte', {
        path,
        maximum: schema.maximum,
      })
    case ValueErrorType.Object:
      return createErr('expected-object', { path })
    case ValueErrorType.ObjectMinProperties:
      return createErr('expected-object-min-props', {
        path,
        minProperties: schema.minProperties,
      })
    case ValueErrorType.ObjectMaxProperties:
      return createErr('expected-object-max-props', {
        path,
        maxProperties: schema.maxProperties,
      })
    case ValueErrorType.ObjectAdditionalProperties:
      return createErr('expected-object-not-to-have-prop', { path })
    case ValueErrorType.ObjectRequiredProperties:
      return createErr('expected-object-required-prop', { path })
    case ValueErrorType.RecordKeyNumeric:
      return createErr('expected-record-numeric-keys', { path })
    case ValueErrorType.RecordKeyString:
      return createErr('expected-record-string-keys', { path })
    case ValueErrorType.String:
      return createErr('expected-string', { path })
    case ValueErrorType.StringMinLength:
      return createErr('expected-string-length-gte', {
        path,
        minLength: schema.minLength,
      })
    case ValueErrorType.StringMaxLength:
      return createErr('expected-string-length-lte', {
        path,
        maxLength: schema.maxLength,
      })
    case ValueErrorType.StringPattern:
      return createErr('expected-string-pattern', {
        path,
        pattern: schema.pattern,
      })
    case ValueErrorType.StringFormat:
      return createErr('expected-string-format', {
        path,
        format: schema.formet,
      })
    case ValueErrorType.Undefined:
      return createErr('expected-undefined', { path })
    case ValueErrorType.Union:
      return createErr('expected-union', { path })
    default:
      return createErr('other', { path, message: error.message })
  }
}
