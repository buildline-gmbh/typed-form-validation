import type { Result } from 'neverthrow'
import { err, ok } from 'neverthrow'

import type { Json, JsonArray, JsonObject } from './json'

export interface FormDataDeserializer<T, ErrorT = unknown> {
  (formData: FormData): Result<T, ErrorT | TypeMismatchError>
}

export function createFormDataDeserializer<T, ErrorT = unknown>(
  cast: (data: Json) => Result<T, ErrorT>,
  options: { ignoreEmptyValues?: boolean } = {}
): FormDataDeserializer<T, ErrorT | TypeMismatchError> {
  return (formData) =>
    formDataToJson(formData, options.ignoreEmptyValues === true).andThen(cast)
}

type FormData = {
  entries(): IterableIterator<[string, unknown]>
}

function formDataToJson(
  formData: FormData,
  ignoreEmptyValues = false
): Result<Json, TypeMismatchError> {
  let entries = [...formData.entries()].filter(
    (e): e is [string, string] => typeof e[1] === 'string'
  )
  if (ignoreEmptyValues) {
    entries = entries.filter(([, value]) => value.length > 0)
  }
  let data: Json | undefined = undefined
  for (const [key, value] of entries) {
    const segments = key.split(/(?<!\\)[[.]/).map((segment) => {
      const isArrayIndex = /(?<!\\)]$/.test(segment)
      return {
        isArrayIndex,
        key: unescapeSegment(isArrayIndex ? segment.slice(0, -1) : segment),
      }
    })
    const result = mergeSegmentData(data, segments, value)
    if (result.isErr()) {
      return result
    }
    data = result.value
  }
  if (typeof data === 'undefined') {
    return err(new TypeMismatchError('something', 'nothing'))
  }
  return ok(data)
}

function unescapeSegment(segment: string): string {
  return segment.replace(/\\([[\].\\])/g, '$1')
}

export class TypeMismatchError extends Error {
  constructor(expected: string, actual: string) {
    super(`Type mismatch: expected ${expected} but saw ${actual}`)
  }
}

function mergeSegmentData(
  parentData: Json | undefined,
  segments: { key: string; isArrayIndex: boolean }[],
  value: string
): Result<Json, TypeMismatchError> {
  if (segments.length < 1) return ok(value)

  const [segment, ...rest] = segments
  if (segment.isArrayIndex) {
    return mergeArraySegmentData(
      parentData as JsonArray | undefined,
      segment,
      rest,
      value
    )
  }
  return mergeObjectSegmentData(
    parentData as JsonObject | undefined,
    segment,
    rest,
    value
  )
}

function mergeArraySegmentData(
  parentData: JsonArray | undefined,
  segment: { key: string; isArrayIndex: boolean },
  rest: { key: string; isArrayIndex: boolean }[],
  value: string
): Result<Json, TypeMismatchError> {
  if (segment.key.length < 1) {
    // scalar array
    if (Array.isArray(parentData) || typeof parentData === 'undefined') {
      return mergeSegmentData(undefined, [], value).andThen((result) =>
        ok([...(parentData ?? []), result])
      )
    }
    return err(new TypeMismatchError('array', typeof parentData))
  }

  // this handles the case where we want to send an empty array via form-data.
  // a key ending with `[_]` tells the parser that this key should always hold
  // an array, even if it is empty. Any following (rest) segments are ignored.
  if (segment.key === '_') {
    return ok(parentData ?? [])
  }

  const index = parseInt(segment.key, 10)
  if (!Number.isFinite(index) || index < 0 || String(index) !== segment.key) {
    return err(new TypeMismatchError('array index (integer)', segment.key))
  }

  if (Array.isArray(parentData) || typeof parentData === 'undefined') {
    return mergeSegmentData(parentData?.[index], rest, value).andThen(
      (result) => {
        const array = parentData ?? []
        array[index] = result
        return ok(array)
      }
    )
  }
  return err(new TypeMismatchError('array', typeof parentData))
}

function mergeObjectSegmentData(
  parentData: JsonObject | undefined,
  segment: { key: string; isArrayIndex: boolean },
  rest: { key: string; isArrayIndex: boolean }[],
  value: string
): Result<Json, TypeMismatchError> {
  if (
    typeof parentData !== 'undefined' &&
    (typeof parentData !== 'object' ||
      parentData === null ||
      Array.isArray(parentData))
  ) {
    return err(new TypeMismatchError('object', typeof parentData))
  }

  return mergeSegmentData(parentData?.[segment.key], rest, value).andThen(
    (result) => ok({ ...parentData, [segment.key]: result })
  )
}
