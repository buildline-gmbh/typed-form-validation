import { useCallback, useState } from 'react'

/**
 * Allows to imperatively force a component to re-render
 *
 * Only really useful when using Refs to keep state and you only want to trigger
 * a re-render when a subcription's data changes.
 *
 * @internal
 */
export function useForceUpdate(): () => void {
  const [, forceUpdate] = useState<any>({})
  return useCallback(() => forceUpdate({}), [])
}
