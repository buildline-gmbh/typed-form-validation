import type { SchemaValidationError } from '@typed-form-validation/core'
import { useCallback, useMemo, useRef } from 'react'

import type { FormDataPath } from '../form-data-path'
import { _internals } from '../internals'
import { useForceUpdate } from './use-force-update'
import { useFormDataSubscription } from './use-form-data-subscription'
import type { FormValueSetter } from './use-form-value'

export function useWriteonlyFormArray<
  Item,
  ErrorT extends SchemaValidationError = SchemaValidationError
>(
  path: FormDataPath<Item[], ErrorT>
): {
  setItems: FormValueSetter<Item[]>
  setAt: (index: number, item: Item) => void
  append: (item: Item) => void
  deleteAt: (index: number) => void
} {
  const { getValueAt, update } = path[_internals]

  const setItems = useCallback<FormValueSetter<Item[]>>(
    (newValue) => {
      const value =
        typeof newValue === 'function' ? newValue(getValueAt(path)) : newValue
      update(path.path, value)
    },
    [getValueAt, path, update]
  )

  const setAt = useCallback(
    (index: number, item: Item) => {
      if (index > 0) {
        update(path.at(index).path, item)
      }
    },
    [path, update]
  )

  const append = useCallback(
    (item: Item) => {
      const value = getValueAt(path)
      if (!value) {
        console.warn(
          `trying to append to an array at ${path.name} that is not contained in the form data`
        )
        return
      }
      update(path.path, value.value.concat(item))
    },
    [getValueAt, path, update]
  )

  const deleteAt = useCallback(
    (index: number) => {
      if (index >= 0) {
        const currentItems = getValueAt(path)
        if (!currentItems) {
          console.warn(
            `trying to remove an item from an array at ${path.name}.${index} that is not contained in the form data`
          )
          return
        }
        if (index >= currentItems.value.length || index < 0) {
          console.warn(
            `trying to remove an item at ${path.name}.${index}, but the array only has ${currentItems.value.length} items`
          )
          return
        }
        update(
          path.path,
          currentItems.value
            .slice(0, index)
            .concat(currentItems.value.slice(index + 1))
        )
      }
    },
    [getValueAt, path, update]
  )

  return useMemo(
    () => ({
      setItems,
      setAt,
      append,
      deleteAt,
    }),
    [append, deleteAt, setAt, setItems]
  )
}

export function useIsFormArrayEmpty(path: FormDataPath<any[], any>): boolean {
  const { getValueAt } = path[_internals]
  const forceUpdate = useForceUpdate()
  const wasEmpty = useRef<boolean>(true)

  const updateValue = useCallback(
    (skipUpdate = false) => {
      const value = getValueAt(path)
      if (!value) return

      const isEmpty = value.value.length < 1
      if (wasEmpty.current === isEmpty) return

      wasEmpty.current = isEmpty

      if (skipUpdate) return

      forceUpdate()
    },
    [forceUpdate, getValueAt, path]
  )

  useMemo(() => updateValue(true), [updateValue])

  useFormDataSubscription(path, updateValue)

  return wasEmpty.current
}

export function useFormArrayItems<Item, ErrorT extends SchemaValidationError>(
  path: FormDataPath<Item[], ErrorT>,
  getKey?: (item: Item) => React.Key
): ReadonlyArray<{
  key: React.Key
  path: FormDataPath<Item, ErrorT>
  index: number
  deleteItem: () => void
}> {
  const { getValueAt } = path[_internals]

  const { deleteAt } = useWriteonlyFormArray<Item, ErrorT>(path)

  const forceUpdate = useForceUpdate()

  const itemsRef = useRef<
    ReadonlyArray<{
      key: React.Key
      path: FormDataPath<Item, ErrorT>
      index: number
      deleteItem: () => void
    }>
  >([])
  useMemo(() => {
    itemsRef.current = getItemObjects(
      getFormInputArrayItemKeys(getValueAt, path, getKey),
      path,
      deleteAt
    )
  }, [deleteAt, getKey, getValueAt, path])

  const handleChange = useCallback(() => {
    const keys = getFormInputArrayItemKeys(getValueAt, path, getKey)
    if (
      itemsRef.current.length !== keys.length ||
      itemsRef.current.some(({ key }, i) => keys[i] !== key)
    ) {
      itemsRef.current = getItemObjects(keys, path, deleteAt)
      forceUpdate()
    }
  }, [deleteAt, forceUpdate, getKey, getValueAt, path])

  useFormDataSubscription(path, handleChange)

  return itemsRef.current
}

function getFormInputArrayItemKeys<
  Item = unknown,
  ErrorT extends SchemaValidationError = SchemaValidationError
>(
  getValueAt: <T>(path: FormDataPath<T, ErrorT>) => { value: T } | null,
  path: FormDataPath<Item[], ErrorT>,
  getKey?: (item: Item) => React.Key
): React.Key[] {
  const items = getValueAt(path)
  if (!items || !Array.isArray(items.value)) return []

  return items.value.map((item, i) => getKey?.(item) ?? i)
}

function getItemObjects<Item, ErrorT extends SchemaValidationError>(
  keys: React.Key[],
  path: FormDataPath<Item[], ErrorT>,
  deleteAt: (index: number) => void
): ReadonlyArray<{
  key: React.Key
  path: FormDataPath<Item, ErrorT>
  index: number
  deleteItem: () => void
}> {
  return keys.map((key, index) => ({
    key,
    path: path.at(index),
    index,
    deleteItem: () => {
      deleteAt(index)
    },
  }))
}
