import type {
  FormPath,
  FormPathKey,
  SchemaValidationError,
  SchemaValidator,
} from '@typed-form-validation/core'
import { produce } from 'immer'
import { ok } from 'neverthrow'
import { useCallback, useMemo, useRef } from 'react'

import type { FormDataPath } from '../form-data-path'
import { createFormDataPath, isFormDataPath } from '../form-data-path'
import type { FormDataInternals } from '../internals'

type Scalar = boolean | number | string | bigint | null | undefined
function isScalar(value: unknown): value is Scalar {
  switch (typeof value) {
    case 'bigint':
    case 'boolean':
    case 'number':
    case 'string':
    case 'symbol':
    case 'undefined':
      return true
    case 'object':
      return value === null
    case 'function':
      return false
  }
}

export function useForm<ValueT>(initialValue: ValueT): FormDataPath<ValueT, any>
export function useForm<ValueT, ErrorT extends SchemaValidationError>(
  initialValue: ValueT,
  validator: SchemaValidator<ValueT, ErrorT>,
  formatErrors: (fieldErrors: ErrorT[]) => string | null
): FormDataPath<ValueT, ErrorT>
export function useForm<ValueT, ErrorT extends SchemaValidationError>(
  initialValue: ValueT,
  validator?: SchemaValidator<ValueT, ErrorT>,
  formatErrors?: (fieldErrors: ErrorT[]) => string | null
): FormDataPath<ValueT, ErrorT> {
  const value = useRef(initialValue)
  const errors = useRef<ErrorT[]>([])

  const subscribers = useRef(new Set<{ path: FormPath; notify: () => void }>())

  const update = useCallback(
    function <T>(path: FormPath, newValue: T) {
      if (path.length < 1) {
        if (value.current === (newValue as unknown as ValueT)) return

        value.current = newValue as unknown as ValueT
      } else {
        const v = produce(value.current, (draft) => {
          let v = draft as any
          for (const key of path.slice(0, -1)) {
            v = v?.[key as keyof typeof v]
          }
          ;(v as Record<any, unknown>)[path.at(-1) as any] = newValue
        })

        if (value.current === v) return
        value.current = v
      }

      const result = validator
        ? validator.validate(value.current)
        : ok(value.current)

      const oldErrors = errors.current
      errors.current = result.isErr() ? result.error : []

      const toNotify = [...subscribers.current].filter(
        subscribersPathFilter(path, oldErrors, errors.current)
      )

      for (const listener of toNotify) {
        listener.notify()
      }
    },
    [validator]
  )

  useMemo(() => {
    if (value.current === initialValue) return

    update([], initialValue)

    value.current = initialValue
    if (!validator) return

    validator.validate(value.current).mapErr((err) => {
      errors.current = err
    })

    // We don't really know what exactly changed for the initial value, so we
    // notify everyone just to be sure.
    ;[...subscribers.current].every((s) => s.notify())

    // Only reset the value when the initialValue prop changes
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [initialValue])

  useMemo(() => {
    if (!validator) {
      errors.current = []
      return
    }

    validator.validate(value.current).mapErr((err) => {
      errors.current = err
    })
  }, [validator])

  const getNameFromPath = useCallback(
    (path: FormPath, options: { skipScalarArraySuffix?: boolean } = {}) => {
      function escapeKeySegment(segment: FormPathKey): string {
        const key = String(segment)
        return key.replace(/([[\].\\])/g, '\\$1')
      }

      const { v, pathName } = path.reduce<{ v: any; pathName: string }>(
        ({ v, pathName }, key, i) => {
          const value = v ?? {}
          const segment = escapeKeySegment(key)
          const suffix = Array.isArray(value)
            ? `[${segment}]`
            : i > 0
            ? `.${segment}`
            : segment
          return {
            v: value[key] as any,
            pathName: pathName + suffix,
          }
        },
        { v: value.current as any, pathName: '' }
      )

      if (options.skipScalarArraySuffix === true) return pathName

      return Array.isArray(v) && (v as any[]).every(isScalar)
        ? pathName + '[]'
        : pathName
    },
    []
  )

  const subscribe = useCallback<FormDataInternals<ErrorT>['subscribe']>(
    (path, notify) => {
      const subscriber = {
        path: isFormDataPath(path) ? path.path : (path as FormPath),
        notify,
      }
      subscribers.current.add(subscriber)
      return () => {
        subscribers.current.delete(subscriber)
      }
    },
    []
  )

  const getValueAt = useCallback(function getValueAt<T>(
    dataPath: FormDataPath<T, ErrorT>
  ): { value: T } | null {
    if (dataPath.path.length < 1)
      return { value: value.current as unknown as T }

    let v = value.current as any
    for (const key of dataPath.path) {
      if (typeof v !== 'object' || v === null || !(key in v)) return null
      v = v[key]
    }
    return { value: v }
  },
  [])

  const getErrorsAt = useCallback(
    (path: FormDataPath<ValueT, ErrorT>): ErrorT[] => {
      if (path.path.length < 1) return errors.current

      return errors.current.filter((e) => {
        const errorPath = e.path
        return (
          path.path.length === errorPath.length &&
          path.path.every((key, i) => String(key) === String(errorPath[i]))
        )
      })
    },
    []
  )

  const internals = useMemo<FormDataInternals<ErrorT>>(
    () => ({
      subscribe,
      update,
      getNameFromPath,
      getValueAt,
      getErrorsAt,
      formatErrors: formatErrors ?? (() => null),
    }),
    [formatErrors, getErrorsAt, getNameFromPath, getValueAt, subscribe, update]
  )

  return useMemo(
    () => createFormDataPath<ValueT, ErrorT>([], internals),
    [internals]
  )
}

function subscribersPathFilter<ErrorT extends SchemaValidationError>(
  path: FormPath,
  oldErrors: ErrorT[],
  newErrors: ErrorT[]
) {
  const pathsToUpdate = [path]
    .concat(oldErrors.map((e) => e.path))
    .concat(newErrors.map((e) => e.path))

  return (subscriber: { path: FormPath }): boolean => {
    return pathsToUpdate.some((updatePath) => {
      const shortestLength = Math.min(updatePath.length, subscriber.path.length)
      return subscriber.path
        .slice(0, shortestLength)
        .every((key, index) => updatePath[index] === key)
    })
  }
}
