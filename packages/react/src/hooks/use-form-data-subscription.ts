import type { SchemaValidationError } from '@typed-form-validation/core'
import { useEffect } from 'react'

import type { FormDataPath } from '../form-data-path'
import { _internals } from '../internals'

/** @internal */
export function useFormDataSubscription<ErrorT extends SchemaValidationError>(
  path: FormDataPath<any, ErrorT>,
  callback: () => void
) {
  useEffect(() => path[_internals].subscribe(path, callback), [callback, path])
}
