import type {
  FormPathKey,
  SchemaValidationError,
} from '@typed-form-validation/core'
import React from 'react'

import type { FormDataPath } from './form-data-path'
import { useFormValue } from './hooks/use-form-value'

export interface DiscriminatedUnionSwitchProps<
  ValueT,
  ErrorT extends SchemaValidationError,
  Discriminator extends keyof ValueT & FormPathKey
> {
  path: FormDataPath<ValueT, ErrorT>
  discriminator: Discriminator
  children: {
    [T in ValueT[Discriminator] & FormPathKey]: (props: {
      path: FormDataPath<Extract<ValueT, Record<Discriminator, T>>>
    }) => React.ReactNode | React.ReactNode[]
  }
}
export function DiscriminatedUnionSwitch<
  ValueT,
  ErrorT extends SchemaValidationError,
  Discriminator extends keyof ValueT & FormPathKey
>({
  path,
  discriminator,
  children,
}: DiscriminatedUnionSwitchProps<ValueT, ErrorT, Discriminator>): JSX.Element {
  const [type] = useFormValue(path.at(discriminator))
  return (
    <>
      {children[type as ValueT[Discriminator] & FormPathKey]({
        path: path as any,
      })}
    </>
  )
}
