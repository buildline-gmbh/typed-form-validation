import type {
  FormPathKey,
  SchemaValidationError,
} from '@typed-form-validation/core'
import React, { useCallback, useMemo, useRef } from 'react'

import type { FormDataPath } from './form-data-path'
import { useFormDataSubscription } from './hooks/use-form-data-subscription'
import { useFormValue, useSetFormValue } from './hooks/use-form-value'
import { _internals } from './internals'

type ChildTypeProps<
  ValueT,
  ErrorT extends SchemaValidationError,
  Discriminator extends keyof ValueT & FormPathKey
> = {
  onChange: (newValue: ValueT[Discriminator] & FormPathKey) => void
  name: string
  errorText: string | null
  errors: ErrorT[]
} & {
  [Variant in ValueT[Discriminator] & FormPathKey]: {
    value: Variant
    path: FormDataPath<Extract<ValueT, Record<Discriminator, Variant>>, ErrorT>
  }
}[ValueT[Discriminator] & FormPathKey]

type DefaultValueMap<
  ValueT,
  Discriminator extends keyof ValueT & FormPathKey
> = {
  [Variant in ValueT[Discriminator] & FormPathKey]: () => Extract<
    ValueT,
    Record<Discriminator, Variant>
  >
}

type DiscriminatedValue<
  T extends ValueT[Discriminator],
  ValueT,
  Discriminator extends keyof ValueT & FormPathKey
> = Extract<ValueT, Record<Discriminator, T>>

type DefaultValueFunction<
  ValueT,
  Discriminator extends keyof ValueT & FormPathKey
> = <T extends ValueT[Discriminator]>(type: T, oldValue: ValueT) => ValueT

type DefaultValueFromType<
  ValueT,
  Discriminator extends keyof ValueT & FormPathKey
> =
  | DefaultValueMap<ValueT, Discriminator>
  | DefaultValueFunction<ValueT, Discriminator>

type NextValueFromTypeParams<
  ValueT,
  Discriminator extends keyof ValueT & FormPathKey
> = {
  [K in ValueT[Discriminator] & FormPathKey]: {
    type: K
    oldValue: ValueT
    cached: { value: DiscriminatedValue<K, ValueT, Discriminator> } | null
  }
}[ValueT[Discriminator] & FormPathKey]

type NextValueFromType<
  ValueT,
  Discriminator extends keyof ValueT & FormPathKey
> = (params: NextValueFromTypeParams<ValueT, Discriminator>) => ValueT

function getDefaultValue<
  T extends ValueT[Discriminator] & FormPathKey,
  ValueT,
  Discriminator extends keyof ValueT & FormPathKey
>(
  source: DefaultValueFromType<ValueT, Discriminator>,
  type: T,
  getOldValue: () => ValueT
): ValueT {
  if (typeof source === 'function') {
    return source(type, getOldValue())
  }
  return source[type]()
}

type UnionKeys<T> = T extends T ? keyof T : never
function getFromObject<T extends object, K extends UnionKeys<T>>(
  value: T,
  key: K
): Extract<T, Record<K, unknown>>[K] | null {
  if (key in value) return (value as Extract<T, Record<K, unknown>>)[key]
  return null
}

export type FormDiscriminatedUnionWrapperProps<
  ValueT,
  ErrorT extends SchemaValidationError,
  Discriminator extends keyof ValueT & FormPathKey
> = {
  path: FormDataPath<ValueT, ErrorT>
  discriminator: Discriminator
  children: (
    props: ChildTypeProps<ValueT, ErrorT, Discriminator>
  ) => React.ReactNode | React.ReactNode[]
} & (
  | { defaultValueFromType: DefaultValueFromType<ValueT, Discriminator> }
  | { nextValueFromType: NextValueFromType<ValueT, Discriminator> }
)
export function FormDiscriminatedUnionWrapper<
  ValueT,
  ErrorT extends SchemaValidationError,
  Discriminator extends keyof ValueT & FormPathKey
>({
  path,
  discriminator,
  children,
  ...remainingProps
}: FormDiscriminatedUnionWrapperProps<
  ValueT,
  ErrorT,
  Discriminator
>): JSX.Element {
  const valuesByDiscriminator = useRef<
    Map<ValueT[Discriminator] & FormPathKey, { value: ValueT }>
  >(new Map())

  const defaultValueFromType = getFromObject(
    remainingProps,
    'defaultValueFromType'
  )
  const nextValueFromType = getFromObject(remainingProps, 'nextValueFromType')

  const [value, , errorText, errors] = useFormValue(path.at(discriminator))
  const setUnionValue = useSetFormValue(path)

  const onChange = useCallback(
    <T extends ValueT[Discriminator] & FormPathKey>(newType: T) => {
      const getCurrentValue = () => {
        const value = path[_internals].getValueAt(path)
        if (!value) throw new Error(`Missing value at ${path.name}`)
        return value.value
      }
      if (defaultValueFromType) {
        const newValue = valuesByDiscriminator.current.get(newType) ?? {
          value: getDefaultValue<T, ValueT, Discriminator>(
            defaultValueFromType,
            newType,
            getCurrentValue
          ),
        }
        if (newValue.value[discriminator] !== newType) {
          const expected = JSON.stringify({ [discriminator]: newType })
          const actual = JSON.stringify({
            [discriminator]: newValue.value[discriminator],
          })

          throw new TypeError(
            `expected a value of type ${expected} but got ${actual}`
          )
        }
        setUnionValue(newValue.value)
      } else if (nextValueFromType) {
        const oldValue = getCurrentValue()
        const cachedValue = (valuesByDiscriminator.current.get(newType) ??
          null) as {
          value: DiscriminatedValue<T, ValueT, Discriminator>
        } | null

        const params = {
          type: newType,
          oldValue,
          cached: cachedValue,
        } as NextValueFromTypeParams<ValueT, Discriminator>
        const newValue = nextValueFromType(params)

        if (newValue[discriminator] !== newType) {
          const expected = JSON.stringify({ [discriminator]: newType })
          const actual = JSON.stringify({
            [discriminator]: newValue[discriminator],
          })

          throw new TypeError(
            `expected a value of type ${expected} but got ${actual}`
          )
        }
        setUnionValue(newValue)
      }
    },
    [
      defaultValueFromType,
      discriminator,
      nextValueFromType,
      path,
      setUnionValue,
    ]
  )

  const { getValueAt } = path[_internals]
  const updateValue = useCallback(() => {
    const newValue = getValueAt(path)
    if (!newValue?.value) return
    valuesByDiscriminator.current.set(
      newValue.value[discriminator] as ValueT[Discriminator] & FormPathKey,
      { value: newValue.value }
    )
  }, [discriminator, getValueAt, path])

  useFormDataSubscription(path, updateValue)
  useMemo(() => updateValue(), [updateValue])

  const child = useMemo(
    () =>
      children({
        onChange,
        value,
        path,
        name: path.at(discriminator).name,
        errorText,
        errors,
      } as unknown as ChildTypeProps<ValueT, ErrorT, Discriminator>),
    [children, discriminator, errorText, errors, onChange, path, value]
  )

  return <>{child}</>
}
